<?php

echo "deve"."lop";          //displays "develop"
echo "<br /><br />";            // html line breaks

echo "deve" . "lop";        //displays "develop"
echo "<br /><br />";            // html line breaks

echo 4 . 3;                     //displays "43"
echo "<br /><br />";            // html line breaks

echo 4.3;                       //displays "4.3"
echo "<br /><br />";            // html line breaks

echo "4" . "3";                //displays "43"
echo "<br /><br />";            // html line breaks

echo '4' . '3';                  //displays "43" 
?>
